Main repository for Sigma Cognitive Architecture
Copyright 2009-2014 University of Southern California


***tips for using git/bitbucket

1. Download/install git: http://git-scm.com/downloads

2. Pull the sigma code onto your machine using the following command:
 
   	git clone https://username@bitbucket.org/sigma-development/sigma-release.git

   using your bitbucket username and password

3. That will copy a directory 'sigma-release' into your local directory, this is our repository

5. For more information on using sigma see the wiki: https://bitbucket.org/sigma-development/sigma-release/wiki/Home
